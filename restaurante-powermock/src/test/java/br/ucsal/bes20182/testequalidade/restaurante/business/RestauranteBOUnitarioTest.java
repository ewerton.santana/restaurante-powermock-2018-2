package br.ucsal.bes20182.testequalidade.restaurante.business;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestauranteBO.class, ComandaDao.class })
public class RestauranteBOUnitarioTest {

	/**
	 * M�todo a ser testado: public static Integer abrirComanda(Integer numeroMesa)
	 * throws RegistroNaoEncontrado, MesaOcupadaException. Verificar se a abertura
	 * de uma comanda para uma mesa livre apresenta sucesso. Lembre-se de verificar
	 * a chamada ao ComandaDao.incluir(comanda).
	 */
	@Before
	public void setup() {
		MesaDao.incluir(new Mesa(1));
	}

	@Test
	public void abrirComandaMesaLivre() throws Exception {
		mockStatic(RestauranteBO.class);
		PowerMockito.doCallRealMethod().when(RestauranteBO.class, "abrirComanda", 1);
		RestauranteBO.abrirComanda(1);
		
		mockStatic(ComandaDao.class);
		PowerMockito.verifyStatic(times(1));
		
		RestauranteBO.abrirComanda(1);
		ComandaDao.incluir(ComandaDao.obterPorCodigo(1));

	}
}
