package br.ucsal.bes20182.testequalidade.restaurante.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import br.ucsal.bes20182.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;

public class ComandaUnitarioTest {

	/**
	 * M�todo a ser testado: private Double calcularTotal(). Verificar o c�lculo
	 * do valor total, com um total de 4 itens.
	 */
	Comanda comanda;
	
	@Before
	public void setup() throws MesaOcupadaException, ComandaFechadaException {
		comanda = new Comanda(new Mesa(1));
		comanda.incluirItem(FabricaItem.umItem().comNome("bebida").comValorUnitario(5D).build(), new Integer(4));
		comanda.incluirItem(FabricaItem.umItem().comNome("bebida").comValorUnitario(10D).build(), new Integer(4));
		comanda.incluirItem(FabricaItem.umItem().comNome("bebida").comValorUnitario(2D).build(), new Integer(2));
		comanda.incluirItem(FabricaItem.umItem().comNome("bebida").comValorUnitario(3D).build(), new Integer(10));
	}
	@Test
	public void calcularTotal4Itens() throws Exception {
		Double valorEsperada = 94D;
		Double valorAtual = Whitebox.invokeMethod(comanda, "calcularTotal");
		Assert.assertEquals(valorEsperada, valorAtual);

	}

}
