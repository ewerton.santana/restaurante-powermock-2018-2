package br.ucsal.bes20182.testequalidade.restaurante.domain;

public class FabricaItem {
	private String nome = "Um nome";
	private Double valorUnitario = 1D;

	public static FabricaItem umItem() {
		return new FabricaItem();
	}
	
	public FabricaItem comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public FabricaItem comValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
		return this;
	}
	
	public Item build() {
		return new Item(nome, valorUnitario);
	}
}
